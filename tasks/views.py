from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Task
from projects.forms import TaskForm


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/my_tasks.html", {"tasks": tasks})


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            project_name = form.cleaned_data.get("project")
            form.instance.project = project_name
            form.save()
            return redirect("list_projects")

    else:
        form = TaskForm()
    return render(request, "tasks/task_create.html", {"form": form})
