from django import forms
from .models import Project
from tasks.models import Task


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]
